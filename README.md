# vac_ctrl_leyboldtd20 conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/vac_ctrl_leyboldtd20"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS vac_ctrl_leyboldtd20 module
